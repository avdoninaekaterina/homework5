package com.homework5.task1;

import java.util.Date;

public class UserProfile {

    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private Integer seriesPassport;
    private Integer numberPassport;

    public UserProfile() {
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setSeriesPassport(Integer seriesPassport) {
        this.seriesPassport = seriesPassport;
    }

    public void setNumberPassport(Integer numberPassport) {
        this.numberPassport = numberPassport;
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                " firstName:'" + firstName + "\'" +
                ", lastName:'" + lastName + "\'" +
                ", dateOfBirth:'" + dateOfBirth + "\'" +
                ", seriesPassport:'" + seriesPassport + "\'" +
                ", numberPassport:'" + numberPassport + "\'" +
                "}";
    }
}
