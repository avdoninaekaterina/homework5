package com.homework5.task1;

import java.util.Date;

public class Passport {
    private Integer seriesPassport;
    private Integer numberPassport;
    private Date dateOfIssueOfPassport;

    public Passport(Integer seriesPassport, Integer numberPassport, Date dateOfIssueOfPassport) {
        this.seriesPassport = seriesPassport;
        this.numberPassport = numberPassport;
        this.dateOfIssueOfPassport = dateOfIssueOfPassport;
    }
}
