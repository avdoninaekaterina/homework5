package com.homework5.task1;

import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;

public class Main {

    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        try {
            Calendar dateOfBirth = new Calendar.Builder().setDate(1997, 7, 7).build();
            Person person = new Person("Ivan", "Ivanov", dateOfBirth.getTime());

            Calendar dateOfIssueOfPassport = new Calendar.Builder().setDate(1997, 7, 7).build();
            Passport passport = new Passport(3316, 2424242, dateOfIssueOfPassport.getTime());

            UserProfile userProfile1 = ObjectCreater.mergeObjects(person, passport, UserProfile.class);
            System.out.println(userProfile1);
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

}
