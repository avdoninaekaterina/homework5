package com.homework5.task1;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class ObjectCreater {

    public static <T> T mergeObjects(Object firstObj, Object secondObj, Class<T> className) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Object mergedClass = className.getDeclaredConstructor().newInstance();
        Field[] fields = mergedClass.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                Field fA = firstObj.getClass().getDeclaredField(field.getName());
                field.setAccessible(true);
                fA.setAccessible(true);
                field.set(mergedClass, fA.get(firstObj));
            } catch (NoSuchFieldException e) {

            }
            try {
                Field fB = secondObj.getClass().getDeclaredField(field.getName());
                field.setAccessible(true);
                fB.setAccessible(true);
                field.set(mergedClass, fB.get(secondObj));
            } catch (NoSuchFieldException e) {
            }
        }
        return (T) mergedClass;
    }
}
