package com.homework5.task3;

import com.homework5.task3.annotation.*;

import java.lang.reflect.Field;

public class Validator {

    public void validateStringLength(Object o) throws IllegalAccessException {
        Class<?> clazz = o.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(Length.class)) {
                Length an = field.getAnnotation(Length.class);
                int number = an.value();
                field.setAccessible(true);
                String value = field.get(o).toString();
                if (value.length() != number) {
                    throw new IllegalStateException(field.getName()
                            + " length should be " + number);
                }
            }
            if (field.isAnnotationPresent(MaxLength.class)) {
                MaxLength an = field.getAnnotation(MaxLength.class);
                int max = an.value();
                field.setAccessible(true);
                String value = field.get(o).toString();
                if (value.length() > max) {
                    throw new IllegalStateException(field.getName()
                            + " length should be less than " + max);
                }
            }
            if (field.isAnnotationPresent(MinLength.class)) {
                MinLength an = field.getAnnotation(MinLength.class);
                int min = an.value();
                field.setAccessible(true);
                String value = field.get(o).toString();
                if (value.length() < min) {
                    throw new IllegalStateException(field.getName()
                            + " length should be greater than " + min);
                }
            }
        }
    }

    public void validateIntSize(Object o) throws IllegalAccessException {
        Class<?> clazz = o.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(Max.class)) {
                Max an = field.getAnnotation(Max.class);
                int max = an.value();
                field.setAccessible(true);
                int value = (int) field.get(o);
                if (value > max) {
                    throw new IllegalStateException(field.getName()
                            + " should be less than " + max);
                }
            }
            if (field.isAnnotationPresent(Min.class)) {
                Min an = field.getAnnotation(Min.class);
                int min = an.value();
                field.setAccessible(true);
                int value = (int) field.get(o);
                if (value < min) {
                    throw new IllegalStateException(field.getName()
                            + " should be greater than " + min);
                }
            }
        }
    }

    public void validateNotEmpty(Object o) throws IllegalAccessException {
        Class<?> clazz = o.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(NotEmpty.class)) {
                field.setAccessible(true);
                String value = field.get(o).toString();
                if (value.isEmpty()) {
                    throw new IllegalStateException(field.getName()
                            + " is empty");
                }
            }
        }
    }

    public void validateNotNull(Object o) throws IllegalAccessException {
        Class<?> clazz = o.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(NotNull.class)) {
                field.setAccessible(true);
                Object value = field.get(o);
                if (value == null) {
                    throw new IllegalStateException(field.getName()
                            + " is null");
                }
            }
        }
    }
}
