package com.homework5.task3;

import com.homework5.task3.entity.Passport;
import com.homework5.task3.entity.Person;

public class Main {
    public static void main(String[] args)  {
        try { //First passport(number(notEmpty and Length(10)) for person( name(2-15), age(14-20) and passport(notNull))
            Validator validator = new Validator();
            Passport passport = new Passport("3333242424");
            validator.validateNotEmpty(passport);
            validator.validateStringLength(passport);

            Person person = new Person("Ivan", 16, passport);
            validator.validateStringLength(person);
            validator.validateIntSize(person);
            validator.validateNotNull(person);
            System.out.println("Success!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
