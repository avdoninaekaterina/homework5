package com.homework5.task3.entity;

import com.homework5.task3.annotation.Length;
import com.homework5.task3.annotation.NotEmpty;

public class Passport {
    @NotEmpty
    @Length(10)
    private String number;

    public Passport(String number) {
        this.number = number;
    }
}
