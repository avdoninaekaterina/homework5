package com.homework5.task3.entity;

import com.homework5.task3.annotation.*;

public class Person {

    @MinLength(2)
    @MaxLength(15)
    private String name;
    @Min(14)
    @Max(20)
    private Integer age;
    @NotNull
    private Passport firstPassport;

    public Person(String name, Integer age, Passport firstPassport) {
        this.name = name;
        this.age = age;
        this.firstPassport = firstPassport;
    }
    
}
