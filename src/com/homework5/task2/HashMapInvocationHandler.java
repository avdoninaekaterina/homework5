package com.homework5.task2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;

public class HashMapInvocationHandler implements InvocationHandler {

    private HashMap<String, Object> hashMap;

    public HashMapInvocationHandler(HashMap<String, Object> hashMap) {
        this.hashMap = hashMap;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long time = System.currentTimeMillis();
        Object result = method.invoke(hashMap, args);
        System.out.println("Method: " + method.getName() + "; Time work: " + (System.currentTimeMillis() - time) + " milliseconds");
        return result;
    }
}
