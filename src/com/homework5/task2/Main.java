package com.homework5.task2;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String args[]) {
        Map proxyMap = (Map) Proxy.newProxyInstance(
                                            Map.class.getClassLoader(),
                                            new Class[]{Map.class},
                                            new HashMapInvocationHandler(new HashMap<>())
                                    );
        proxyMap.put("United Kingdom", 0);
        proxyMap.put("Austria", 1);
        proxyMap.put("Belarus", 2);
        proxyMap.put("Turkey", 2);
        proxyMap.get("Austria");
        proxyMap.put("Russia", 3);
        proxyMap.size();
        proxyMap.remove("Turkey");
        proxyMap.size();

    }

}
